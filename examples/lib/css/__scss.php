<?php

ini_set('memory_limit','300M');

header('Content-type: text/css');

require '../util/scssphp/scss.inc.php';
	
use Leafo\ScssPhp\Compiler;
use Leafo\ScssPhp\Formatter\Compact;
use Leafo\ScssPhp\Formatter\Compressed;
use Leafo\ScssPhp\Formatter\Crunched;

$scss = new Compiler();
$scss->setFormatter('Leafo\ScssPhp\Formatter\Compact');

try {
	
	$css = $scss->compile('@import "_theme/theme.scss"');
	
	file_put_contents("compile-framework.css", $css);

	echo 'Compiled @ ' .date('G:i:s');
	
} catch (exception $e){
	echo "fatal error: " . $e->getMessage();
}
