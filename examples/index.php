<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Collection of extension classes for the latest Bootstrap version.">
    <meta name="author" content="Jacob Abshire">
    <title>Bootstrap Extension SASS</title>

    <!-- Bootstrap Core & Extensions Compiled -->
    <link href="/lib/css/compile-framework.css" rel="stylesheet">
</head>
<body class="pt-5">

<!--header-->
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#top">Top</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#something">Something</a>
            </li>
        </ul>
    </div>
</nav>

<!--main-->
<main role="main" class="container pt-5">

    <!--heading-->
    <div class="mb-5">
        <h1>Bootstrap Extension SASS Examples</h1>
        <p class="lead">Usst.</p>
    </div>

    <!--example-->
    <div class="mb-4">
        <h2 class="mb-3">Typography</h2>
        <div class="row mb-4">

            <!--grays-->
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        Grays
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li><span class="text-gray-100">text-gray-100</span></li>
                            <li><span class="text-gray-200">text-gray-200</span></li>
                            <li><span class="text-gray-300">text-gray-300</span></li>
                            <li><span class="text-gray-400">text-gray-400</span></li>
                            <li><span class="text-gray-500">text-gray-500</span></li>
                            <li><span class="text-gray-600">text-gray-600</span></li>
                            <li><span class="text-gray-700">text-gray-700</span></li>
                            <li><span class="text-gray-800">text-gray-800</span></li>
                            <li><span class="text-gray-900">text-gray-900</span></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!--colors-->
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        Colors
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li><span class="text-blue">text-blue</span></li>
                            <li><span class="text-indigo">text-indigo</span></li>
                            <li><span class="text-purple">text-purple</span></li>
                            <li><span class="text-pink">text-pink</span></li>
                            <li><span class="text-red">text-red</span></li>
                            <li><span class="text-orange">text-orange</span></li>
                            <li><span class="text-yellow">text-yellow</span></li>
                            <li><span class="text-green">text-green</span></li>
                            <li><span class="text-cyan">text-cyan</span></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!--colors-->
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Link Hover Colors
                    </div>
                    <div class="card-body d-flex justify-contents-start align-items-start">
                        <ul class="list-unstyled mr-5">
                            <li><a href="#" class="text-hover-primary">text-primary</a></li>
                            <li><a href="#" class="text-hover-secondary">text-secondary</a></li>
                            <li><a href="#" class="text-hover-success">text-success</a></li>
                            <li><a href="#" class="text-hover-info">text-info</a></li>
                            <li><a href="#" class="text-hover-warning">text-warning</a></li>
                            <li><a href="#" class="text-hover-danger">text-danger</a></li>
                            <li><a href="#" class="text-hover-light">text-light</a></li>
                            <li><a href="#" class="text-hover-dark">text-dark</a></li>
                        </ul>
                        <ul class="list-unstyled mr-5">
                            <li><a href="#" class="text-hover-gray-100">text-gray-100</a></li>
                            <li><a href="#" class="text-hover-gray-200">text-gray-200</a></li>
                            <li><a href="#" class="text-hover-gray-300">text-gray-300</a></li>
                            <li><a href="#" class="text-hover-gray-400">text-gray-400</a></li>
                            <li><a href="#" class="text-hover-gray-500">text-gray-500</a></li>
                            <li><a href="#" class="text-hover-gray-600">text-gray-600</a></li>
                            <li><a href="#" class="text-hover-gray-700">text-gray-700</a></li>
                            <li><a href="#" class="text-hover-gray-800">text-gray-800</a></li>
                            <li><a href="#" class="text-hover-gray-900">text-gray-900</a></li>
                        </ul>
                        <ul class="list-unstyled">
                            <li><a href="#" class="text-hover-blue">text-hover-blue</a></li>
                            <li><a href="#" class="text-hover-indigo">text-hover-indigo</a></li>
                            <li><a href="#" class="text-hover-purple">text-hover-purple</a></li>
                            <li><a href="#" class="text-hover-pink">text-hover-pink</a></li>
                            <li><a href="#" class="text-hover-red">text-hover-red</a></li>
                            <li><a href="#" class="text-hover-orange">text-hover-orange</a></li>
                            <li><a href="#" class="text-hover-yellow">text-hover-yellow</a></li>
                            <li><a href="#" class="text-hover-green">text-hover-green</a></li>
                            <li><a href="#" class="text-hover-cyan">text-hover-cyan</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <div class="row mb-4">
            <!--sizes-->
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Text Sizes
                        <div>
                            The use of gridpoints allowed: <code>.text-md-50</code>
                        </div>
                    </div>
                    <div class="card-body d-flex justify-contents-start">
                        <ul class="list-unstyled mr-5">
                            <li><span class="text-50">text-50</span></li>
                            <li><span class="text-60">text-60</span></li>
                            <li><span class="text-70">text-70</span></li>
                            <li><span class="text-80">text-80</span></li>
                            <li><span class="text-90">text-90</span></li>
                            <li><span class="text-100">text-100</span></li>
                            <li><span class="text-110">text-110</span></li>
                            <li><span class="text-120">text-120</span></li>
                            <li><span class="text-130">text-130</span></li>
                            <li><span class="text-140">text-140</span></li>
                            <li><span class="text-150">text-150</span></li>
                        </ul>
                        <ul class="list-unstyled mr-5">
                            <li><span class="text-160">text-160</span></li>
                            <li><span class="text-170">text-170</span></li>
                            <li><span class="text-180">text-180</span></li>
                            <li><span class="text-190">text-190</span></li>
                            <li><span class="text-200">text-200</span></li>
                            <li><span class="text-210">text-210</span></li>
                            <li><span class="text-220">text-220</span></li>
                            <li><span class="text-230">text-230</span></li>
                            <li><span class="text-240">text-240</span></li>
                            <li><span class="text-250">text-250</span></li>
                        </ul>
                        <ul class="list-unstyled mr-5">
                            <li><span class="text-260">text-260</span></li>
                            <li><span class="text-270">text-270</span></li>
                            <li><span class="text-280">text-280</span></li>
                            <li><span class="text-290">text-290</span></li>
                            <li><span class="text-300">text-300</span></li>
                            <li><span class="text-400">text-400</span></li>
                            <li><span class="text-500">text-500</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main><!-- /.container -->

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


</html>